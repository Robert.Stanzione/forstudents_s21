import library
import patron
import book

# this is a simple function just to exercise my code
# write one of these as you develop your code, so that you can call your functions
# if this were a real system, then this would be the actual product code

#----------------------------------------

def simple_runtime_test(thelib):
    mary = patron.Patron('Mary')
    john = patron.Patron('John')
    thelib.add_patron(mary)
    thelib.add_patron(john)

    title_1 = 'Book One Title'
    title_2 = 'Book Two Title'
    book_1 = book.Book(title_1, 'Author Person 1', '800.1')
    book_2 = book.Book(title_2, 'Author Person 2', '800.2')
    thelib.add_book(book_1)
    thelib.add_book(book_2)

    name_to_look_for = 'Mary'
    p = thelib.find_patron_by_name(name_to_look_for)
    if p is not None:
        books = thelib.find_book(title_1)
        if len(books) > 0:
            print('check out ' + books[0].to_string() + ' to ' + p.to_string())
            thelib.do_checkout(p, books[0])
        else:
            print('cannot find book with title ' + title_1)

        books = thelib.find_book(title_2)
        if len(books) > 0:
            print('check out ' + books[0].to_string() + ' to ' + p.to_string())
            thelib.do_checkout(p, books[0])
        else:
            print('cannot find book with title ' + title_2)
    else:
        print('cannot find patron with name', name_to_look_for)

    print('here are the checkouts for the library:')
    thelib.show_checkouts()

    card_to_look_for = 2
    p = thelib.find_patron_by_card(card_to_look_for)
    if p is not None:
        books = thelib.get_checkouts(p)
        if len(books) > 0:
            print('return ' + books[0].to_string() + ' to ' + p.to_string())
    else:
        print('cannot find patron with card #', card_to_look_for)

    print('here are the checkouts for the library')
    thelib.show_checkouts()

#----------------------------------------

def main():
  mylib = library.Library().get()

  simple_runtime_test(mylib)

#----------------------------------------

main()
