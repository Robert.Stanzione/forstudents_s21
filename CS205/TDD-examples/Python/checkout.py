# checkout provides the association between a book and a patron
# when a patron checks out a book, an instance of Checkout is
# created--it's like a "circulation record" showing that a patron
# has a book

class Checkout:
  def __init__(self, patron, book):
    self.patron = patron
    self.book = book

  def get_book(self):
    return self.book

  def get_patron(self):
    return self.patron
