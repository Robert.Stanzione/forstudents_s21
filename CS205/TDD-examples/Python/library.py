import checkout

# this uses a design pattern called the singleton pattern:
# define a static instance of the class itself and return
# that instance in a getter
#
# you don't have to use this pattern, but it's a elegant
# way to define a container class
#
# when a patron checks out a book, an instance of Checkout is created
# to show make the association between the patron and the book;
# the book is also added to the patron's list of books

class Library:
    s_library = None

    @classmethod
    def get(self):
        if self.s_library is None:
            self.s_library = Library()
        return self.s_library

    def __init__(self):
        self.books = set()
        self.patrons = set()
        self.checkouts = set()

    def add_patron(self, patron):
        self.patrons.add(patron)

    def get_patrons(self):
        return self.patrons

    def add_book(self, book):
        self.books.add(book)

    def get_books(self):
        return self.books

    def find_patron_by_card(self, card_number):
        for p in self.patrons:
            if p.get_card_number() == card_number:
                return p
        return None

    def find_patron_by_name(self, name):
        for p in self.patrons:
            if p.get_name() == name:
                return p
        return None

    def find_book(self, title):
        books = []
        for b in self.books:
            if b.get_title() == title:
                books.append(b)
        return books

    def do_checkout(self, patron, book):
        if not self.is_checked_out(book):
            co = checkout.Checkout(patron, book)
            patron.do_checkout(book)
            self.checkouts.add(co)
            return co
        else:
            return None

    def is_checked_out(self, book):
        for c in self.checkouts:
            if c.get_book() == book:
                return True
        return False

    def show_checkouts(self):
        for c in self.checkouts:
            s = c.get_patron().to_string() + ' => ' + c.get_book().to_string()
            print(s)

    def get_checkouts(self, p):
        booklist = []
        for c in self.checkouts:
            if c.get_patron() == p:
                booklist.append(c.get_book())
        return booklist

    # here is an incorrect implementation of "return book"

    def return_book_incorrect_implementation(self, patron, book):
        # this is incorrect because I am removing the book from the library's checkouts
        # but not from the patron's checkouts
        for c in self.checkouts:
            if c.get_patron() == patron and c.get_book() == book:
                self.checkouts.remove(c)
                return True
        return False

    # and here is the correct implementation of "return book"

    def return_book(self, patron, book):
        # the correct implementation
        for co in self.checkouts:
            if co.get_patron() == patron and co.get_book() == book:
                self.checkouts.remove(co)
                patron.return_book(book)
                return True
        return False

    def return_all_books(self):
        for p in self.patrons:
            p.checkedout_books = set()
        self.checkouts = set()
