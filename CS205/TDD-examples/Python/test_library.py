import unittest
import library
import book
import patron

# unit tests for my Library application
#
# Define a class that is subclassed from unittest.Testcase
#
# There are four functions you can implement
#
# (1) static member function setUpClass()
# - this will be called one time, before any tests run
# - the cls parameter will be an instance of this class:
#   it's how you can refer to member variables
# - this is a useful one--you can set things up here for your tests
#
# (2) static member function tearDownClass()
# - do any cleanup you need (you might not need to do any; so in this case
#   you don't have to implement the function
#
# (3) setUp()
# - called before each test
# - if you don't need to do anything before each test, then don't implement this
#
# (4) tearDown()
# - called after each test
# - I use this to clean up after each test
# - but again, if you don't need to do any cleanup after each tests,
#   then don't implement this
#
# in PyCharm, you can run your tests by right-clicking on your test file
# and then doing "Run Unittests in [your filename]
#
# View -> Tool Windows -> Run will show you the results of your tests

class TestCheckout(unittest.TestCase):
    library = None

    @classmethod
    def setUpClass(cls):
        # called one time, at the very beginning
        print('setUpClass()')
        cls.library = library.Library().get()

        # create a few books and patrons
        title_1 = 'Book One Title'
        title_2 = 'Book Two Title'
        title_3 = 'Book Three Title'
        author_a = 'Author Person A'
        author_b = 'Author Person B'
        author_c = 'Author Person C'
        # we'll use the books and the patrons in the tests, so make them class variables
        cls.book_1 = book.Book(title_1, author_a, '800.1')
        cls.book_2 = book.Book(title_2, author_b, '800.2')
        cls.book_3 = book.Book(title_3, author_c, '800.3')
        cls.john = patron.Patron('John')
        cls.mary = patron.Patron('Mary')
        cls.library.add_patron(cls.john)
        cls.library.add_patron(cls.mary)
        cls.library.add_book(cls.book_1)
        cls.library.add_book(cls.book_2)
        cls.library.add_book(cls.book_3)

    @classmethod
    def tearDownClass(cls):
        # called one time, at the very end--if you need to do any final cleanup, do it here
        print('tearDownClass()')

    def setUp(self):
        # called before every test
        print('setUp()')

    def tearDown(self):
        # called after every test
        print('tearDown()')
        self.library.return_all_books()

    # -------------------------------------------------------------

    def test_return_incorrect(self):
        # this tests the incorrect version of return_book to library: this test will fail

        # check out a book to John
        self.library.do_checkout(self.john, self.book_1)
        # return john's book--should return True
        rc = self.library.return_book_incorrect_implementation(self.john, self.book_1)
        self.assertTrue(rc)

        # try to return the same book again--should return False
        rc = self.library.return_book_incorrect_implementation(self.john, self.book_1)
        self.assertFalse(rc)

        # check that the library shows that john has no books checked out
        books = self.library.get_checkouts(self.john)
        self.assertEqual(len(books), 0)

        # check that john shows no books checked out
        # right now, this code is failing, because my code is incorrect--
        # I'm returning to the library but not to Patron
        books = self.john.get_checkouts()
        self.assertEqual(len(books), 0)

    #-------------------------------------------------------------

    def test_return_correct(self):
        print('test return correct')
        # this tests the correct implemenation of return book to library: it will succeeed

        # check out a book to John
        self.library.do_checkout(self.john, self.book_1)
        # return john's book--should return True
        rc = self.library.return_book(self.john, self.book_1)
        self.assertTrue(rc)

        # try to return the same book again--should return False
        rc = self.library.return_book(self.john, self.book_1)
        self.assertFalse(rc)

        # check that the library shows that john has no books checked out
        books = self.library.get_checkouts(self.john)
        self.assertEqual(len(books), 0)

        # check that john shows no books checked out
        # this will pass
        books = self.john.get_checkouts()
        self.assertEqual(len(books), 0)

    #-------------------------------------------------------------

    def test_checkout_one(self):
        # check that the library shows that no books are checked out to john
        books = self.library.get_checkouts(self.john)
        self.assertEqual(len(books), 0)

        # check out a book to john
        c = self.library.do_checkout(self.john, self.book_1)

        # check that the library shows one book checked out to john
        books = self.library.get_checkouts(self.john)
        self.assertEqual(len(books), 1)

        # check that the book checked out to john is book1
        if len(books) == 1:
            self.assertEqual(books[0], self.book_1)

        # check that john shows one book checked out
        books = self.john.get_checkouts()
        self.assertEqual(len(books), 1)

        # check that the book checked out to john is book1
        if len(books) == 1:
            self.assertEqual(books[0], self.book_1)

    #-------------------------------------------------------------

    def test_checkout_two(self):
        # check out a book to mary
        c = self.library.do_checkout(self.mary, self.book_2)
        self.assertIsNotNone(c)

        # check that the library shows one book checked out to mary
        books = self.library.get_checkouts(self.mary)
        self.assertEqual(len(books), 1)

        # check that the book checked out to mary is book_2
        if len(books) == 1:
            self.assertEqual(books[0], self.book_2)

        # check that mary shows one book checked out
        books = self.mary.get_checkouts()
        self.assertEqual(len(books), 1)

        # check that the book checked out to mary is book2
        if len(books) == 1:
            self.assertEqual(books[0], self.book_2)

    # -------------------------------------------------------------

    def test_checkout_three(self):
        # try checking a book out to mary and then the same book to John: this should fail

        # check out a book to mary
        c = self.library.do_checkout(self.mary, self.book_1)
        self.assertIsNotNone(c)

        # check out a book to john
        c = self.library.do_checkout(self.john, self.book_2)
        self.assertIsNotNone(c)

        # now try checking out to john the book that mary has already checked out
        # this should fail
        c = self.library.do_checkout(self.john, self.book_1)
        self.assertIsNone(c)

#==================================

if __name__ == "__main__":
    unittest.main()
