# patron: a person who will use the library
# when a patron checks out a book, the book is saved in a set that patron owns

class Patron:
  card_number: int
  s_next_card_number = 0

  def __init__(self, name):
    self.name = name
    Patron.s_next_card_number = Patron.s_next_card_number + 1
    self.card_number = Patron.s_next_card_number
    self.checkedout_books = set()

  def to_string(self):
    s = self.name + ' id=' + str(self.card_number) + '; #checkout(s) = ' + \
          str(len(self.checkedout_books))
    return s

  def get_card_number(self):
    return self.card_number

  def get_name(self):
    return self.name
  
  def __eq__(self, other):
    return self.card_number == other.card_number

  def __hash__(self):
    return hash((self.name, self.card_number))

  def do_checkout(self, book):
    self.checkedout_books.add(book)

  def get_checkouts(self):
    return list(self.checkedout_books)

  def return_book(self, book):
    for b in self.checkedout_books:
      if b == book:
        self.checkedout_books.remove(book)
        return True
    print('tried to return book', book, 'to patron', self, ': failed')
    return False