import java.util.ArrayList;
import java.util.List;

class Patron {
  private static int nextCardNumber = 0;
  
  private String name;
  private int cardNumber;
  private List<Book> checkouts;
  
  private Patron() {
  }
  
  public Patron(String name) {
    this.name = name;
    ++nextCardNumber;
    this.cardNumber = nextCardNumber;
    this.checkouts = new ArrayList<> ();
  }
  
  public String toString() {
    String s = this.name + " " + this.cardNumber
        + "; #checkout(s) = " + this.checkouts.size();
    return s;
  }
  
  public int getCardNumber() {
    return this.cardNumber;
  }
  
  public boolean equals(Object obj) {
    if (obj != null && getClass() == obj.getClass()) {
      Patron p = (Patron) obj;
      return this.cardNumber == p.cardNumber;
    }
    return false;
  }
  
  public void doCheckout(Book b) {
    this.checkouts.add(b);
  }
  
  public List<Book> getCheckouts() {
    return this.checkouts;
  }
}