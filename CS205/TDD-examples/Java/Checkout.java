class Checkout {
  private Book book;
  private Patron patron;
  
  private Checkout() {
  }
  
  public Checkout(Patron patron, Book book) {
    this.book = book;
    this.patron = patron;
  }
  
  public Book getBook() {
    return this.book;
  }
  
  public Patron getPatron() {
    return this.patron;
  }
}