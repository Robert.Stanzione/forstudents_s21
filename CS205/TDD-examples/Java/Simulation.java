import java.util.List;
import java.util.ArrayList;

public class Simulation {
  public Simulation(Library library) {
    library.addPatron(new Patron("John"));
    library.addPatron(new Patron("Mary"));
    
    library.addBook(new Book("Book One Title", "Author Person 1", "800.1"));
    library.addBook(new Book("Book Two Title", "Author Person 2", "800.2"));

//     List<Patron> patrons = library.getPatrons();
//     for (Patron patron : patrons) {
//       System.out.println(patron.toString());
//     }
//     
//     List<Book> books = library.getBooks();
//     for (Book book : books) {
//       System.out.println(book.toString());
//     }
    
    Patron patron = library.findPatron(1);
    if (patron != null) {
      List<Book> books = library.findBook("Book One Title");
      if (books.size() > 0) {
        library.doCheckout(patron, books.get(0));
      }
      books = library.findBook("Book Two Title");
      if (books.size() > 0) {
        library.doCheckout(patron, books.get(0));
      }
    }
    
    System.out.println("here are the checkouts for the library:");
    library.showCheckouts();
    
    patron = library.findPatron(1);
    if (patron != null) {
      List<Book> books = library.getCheckouts(patron);
      if (books.size() > 0) {
        library.returnBook(patron, books.get(0));
      }
    }
    
    System.out.println("here are the checkouts for the library:");
    library.showCheckouts();
  }
}