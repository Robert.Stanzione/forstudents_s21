class BookSet {
  private String title;
  private String author;
  private String catalogNumber;
  private int numCopies;

  private Book() {
  }
  
  public Book(String title, String author, String catalogNumber) {
    this(title, author, catalogNumber, 1);
  }
  
  public Book(String title, String author, String catalogNumber, int copyNumber) {
    this.title = title;
    this.author = author;
    this.catalogNumber = catalogNumber;
    this.copyNumber = copyNumber;
  }
  
  public String toString() {
    String s = "\"" + this.title + "\" " + this.author + " "
      + this.catalogNumber + " " + " copy " + this.copyNumber;
    return s;
  }
  
  public String getTitle() {
    return this.title;
  }
  
  public boolean equals(Object obj) {
    if (obj != null && getClass() == obj.getClass()) {
      Book b = (Book) obj;
      return this.title == b.title &&
        this.author == b.author &&
        this.copyNumber == b.copyNumber;
    }
    return false;
  }
  
  public boolean equalsWithoutCopyNumber(Object obj) {
    if (obj != null && getClass() == obj.getClass()) {
      Book b = (Book) obj;
      return this.title == b.title &&
        this.author == b.author;
    }
    return false;
  }
  
  public int getNumCopies() {
    return 
}