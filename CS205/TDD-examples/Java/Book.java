class Book {
  private String title;
  private String author;
  private String catalogNumber;
  // copyNumber would make this too complicated

  private Book() {
  } 

  public Book(String title, String author, String catalogNumber) {
    this.title = title;
    this.author = author;
    this.catalogNumber = catalogNumber;
  }

  public String toString() {
    String s = "\"" + this.title + "\" " + this.author + " "
      + this.catalogNumber;
    return s;
  }
  
  public String getTitle() {
    return this.title;
  }
  
  public boolean equals(Object obj) {
    if (obj != null && getClass() == obj.getClass()) {
      Book b = (Book) obj;
      return this.title == b.title && this.author == b.author;
    }
    return false;
  }
}
