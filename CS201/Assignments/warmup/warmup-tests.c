//---------------------------------------
// jdh CS201 Spring 2021
// tests for assigment #1 (warmup)
//---------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "warmup.jhibbele.h"

int main() {
  char firstName[256], lastName[256];
  StudentData *record1, *record2, *record3;
  int rc, count, fail, result;
  int id;
  char s[64];

  // test the first function

  fail = 0;
  strcpy(s, "now is the time for all good men");
  rc = countChars(s, 't', &count);
  if (rc != 1) {
    printf("ERROR: rc should be 1 for first test\n");
    fail = 1;
  }
  if (count != 2) {
    printf("ERROR: count should be 2 for first test\n");
    fail = 1;
  }
  if (fail == 0) {
    printf("Correct! First test passed.\n");
  }

  fail = 0;
  rc = countChars(s, 'q', &count);
  if (rc != 0) {
    printf("ERROR: rc should be 0 for second test\n");
    fail = 1;
  }
  if (count != 0) {
    printf("ERROR: count should be 0 for second test\n");
    fail = 1;
  }
  if (fail == 0) {
    printf("Correct! Second test passed.\n");
  }

  // now test the other two functions

  fail = 0;
  strcpy(firstName, "Ira");
  strcpy(lastName, "Allen");
  id = 189;
  rc = createRecord(firstName, lastName, id, &record1);
  if (rc != 0) {
    printf("ERROR: got rc != 0 for first test of createRecord()\n");
    fail = 1;
  }

  if (record1->id != id) {
    printf("ERROR: id in record is not correct\n");
    fail = 1;
  }
  if ( strcmp(record1->firstName, firstName) ) {
    printf("ERROR: firstName in record is not correct\n");
    fail = 1;
  }
  if ( strcmp(record1->lastName, lastName) ) {
    printf("ERROR: lastName in record is not correct\n");
    fail = 1;
  }
  if (fail == 0) {
    printf("Correct! Third test passed.\n");
  }

  fail = 0;
  strcpy(firstName, "A_very_extra_long_name_that_is_too_long");
  strcpy(lastName, "Allen");
  id = 19;
  rc = createRecord(firstName, lastName, id, &record2);
  if (rc != 1) {
    printf("ERROR: got rc == 0 for second test of createRecord()\n");
  } else {
    printf("Correct! Fourth test passed.\n");
  }

  fail = 0;
  strcpy(firstName, "Ira");
  strcpy(lastName, "Allen");
  id = 189;
  // assume that this one works correctly (don't check return code)
  createRecord(firstName, lastName, id, &record3);

  rc = compareRecords(record1, NULL, &result);
  if (rc != 1) {
    printf("ERROR: got rc == 0 for first test of compareRecords()\n");
  } else {
    printf("Correct! Fifth test passed.\n");
  }

  fail = 0;
  rc = compareRecords(record1, record3, &result);
  if (rc != 0) {
    printf("ERROR: got rc != 0 for second test of compareRecords()\n");
    fail = 1;
  }
  if (result != 1) {
    printf("ERROR: got result != 1 for second test of compareRecords()\n");
    fail = 1;
  }

  if (fail == 0) {
    printf("Correct! Sixth test passed.\n");
  }

  return 0;
}
