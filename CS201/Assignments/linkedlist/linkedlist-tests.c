//---------------------------------------
// jdh CS201 Spring 2021
// tests for assigment #2 (linked list)
//---------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedlist.jhibbele.h"

int createAndInsert(ListNode **theList, char *first, char *last, int id) {
  StudentData *record;
  int rc;

  record = (StudentData *) malloc(sizeof(StudentData));
  strcpy(record->firstName, first);
  strcpy(record->lastName, last);
  record->id = id;

  rc = insertRecord(theList, record);
  return rc;
}

//----------------------------------------------------------------------

int main() {
  ListNode *theList;
  StudentData *record;
  int rc;
  char first[MAX_NAME_LENGTH];
  char last[MAX_NAME_LENGTH];
  int id;

  theList = NULL;

  printf("first test some insertions\n\n");
  // create Rafael Nadal 37
  strcpy(first, "Rafael");
  strcpy(last, "Nadal");
  id = 37;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  // create Ang Lee 21
  strcpy(first, "Ang");
  strcpy(last, "Lee");
  id = 21;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  // create Ingmar Bergman 45
  strcpy(first, "Ingmar");
  strcpy(last, "Bergman");
  id = 45;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  // create Thomas Hanks 29
  strcpy(first, "Thomas");
  strcpy(last, "Hanks");
  id = 29;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  // create Breanna Stewart 30
  strcpy(first, "Breanna");
  strcpy(last, "Stewart");
  id = 30;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  // now try to insert a different student with id=29
  strcpy(first, "Taylor");
  strcpy(last, "Swift");
  id = 29;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("ERROR: zero return code trying to add %s %s %d\n", first, last, id);
  } else {
    printf("Correct: unable to add %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

  printf("now test some lookups\n\n");

  id = 29;
  record = fetchRecord(theList, id);
  if (record != NULL) {
    printf("Correct: found '%s' '%s' %d for record with id=%d\n",
           record->firstName, record->lastName, record->id, id);
  } else {
    printf("ERROR: should have found record for id=%d\n", id);
  }

  id = 290;
  record = fetchRecord(theList, id);
  if (record != NULL) {
    printf("ERROR: get record '%s' '%s' %d id=%d\n",
           record->firstName, record->lastName, record->id, id);
  } else {
    printf("Correct: did not fetch record for id=%d\n", id);
  }

  printf("\n");

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

  printf("now test deletions\n\n");

  // try to delete id=29
  id = 29;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("Correct: deleted record with id=%d\n", id);
  } else {
    printf("ERROR: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=21 (the first node in the list)
  id = 21;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("Correct: deleted record with id=%d\n", id);
  } else {
    printf("ERROR: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=45
  id = 45;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("Correct: deleted record with id=%d\n", id);
  } else {
    printf("ERROR: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=300 (doesn't exist)
  id = 300;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("ERROR: zero return code from deleting record with id=%d\n", id);
  } else {
    printf("Correct: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=30
  id = 30;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("Correct: deleted record with id=%d\n", id);
  } else {
    printf("ERROR: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=37
  id = 37;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("Correct: deleted record with id=%d\n", id);
  } else {
    printf("ERROR: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // try to delete id=30: should fail, because list is empty
  id = 30;
  rc = deleteRecord(&theList, id);
  if (rc == 0) {
    printf("ERROR: zero return code from deleting record with id=%d\n", id);
  } else {
    printf("Correct: failed to delete record with id=%d\n", id);
  }

  printList(theList);
  printf("\n");

  // now add a new record
  strcpy(first, "Teemu");
  strcpy(last, "Selanne");
  id = 8;

  rc = createAndInsert(&theList, first, last, id);
  if (rc == 0) {
    printf("Correct: inserted %s %s %d\n", first, last, id);
  } else {
    printf("ERROR: nonzero return code trying to insert  %s %s %d\n", first, last, id);
  }

  printList(theList);
  printf("\n");

  return 0;
}
