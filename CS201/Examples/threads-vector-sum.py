# 2-10-19 simple threads in python

from random import random
import threading

def worker(thread_num, A, B, C, start_index, end_index):
    print('task ' + str(thread_num) + ' is starting, start = ' +
          str(start_index) + ' end = ' + str(end_index))
    for i in range(start_index, end_index+1):
        C[i] = A[i] + B[i]

#-------------------------------------------------------------

def add(A, B, C):
    thread_list = []

    n = len(A)

    numtasks = 4
    chunk = n // numtasks
    leftover = n % numtasks
    for i in range(numtasks-1):
      start = i*chunk
      end = start + chunk - 1
      t = threading.Thread(target=worker, args=(i, A, B, C, start, end))
      t.start()
      thread_list.append(t)

    start = (numtasks-1)*chunk
    end = n-1
    t = threading.Thread(target=worker, args=(numtasks-1, A, B, C, start, end))
    t.start()
    thread_list.append(t)

    for t in thread_list:
      t.join()

#-------------------------------------------------------------

def main():
  n = 110
  A = []
  B = []
  C = [0] * n
  for i in range(n):
    A.append(random())
    B.append(1 - A[i])

  add(A, B, C)

  err = False
 
  for i in range(n):
    if abs(C[i] - 1.0) > 1e-10:
      print('error: C[' + str(i) + '] = ' + str(C[i]))
      err = True

  if not err:
    print('no errors!')

#-------------------------------------------------------------

main()
