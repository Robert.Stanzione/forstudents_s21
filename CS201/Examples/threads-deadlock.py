from time import sleep
import threading
from random import randint

def do_work_bad(tnum, lock1, lock2):
    print('task', tnum, 'is starting')

    if tnum % 2 == 0:
        even = True
        sleep(1/10000)
    else:
        even = False

    for i in range(4):
        if even:
            lock1.acquire()
            lock2.acquire()
        else:
            lock2.acquire()
            lock1.acquire()

        ival = 5
        print('task', tnum, 'is in its critical section; sleep for', 100*ival, ' ms')
        sleep(ival/10)

        if even:
            lock2.release()
            lock1.release()
        else:
            lock1.release()
            lock2.release()

        print('task ', tnum, ' is past its critical section ')
        
    return True

#=========================================================================

def do_work_good(tnum, lock1, lock2):
    print('task', tnum, 'is starting')

    if tnum % 2 == 0:
        even = True
    else:
        even = False
        
    for i in range(4):
        if even:
            done = False
            while not done:
                if not lock1.locked() and not lock2.locked():
                    lock1.acquire()
                    lock2.acquire()
                    done = True
                else:
                    print('task',tnum, ': wait for locks')
                    sleep(1)
        else:
            done = False
            while not done:
                if not lock1.locked() and not lock2.locked():
                    lock1.acquire()
                    lock2.acquire()
                    done = True
                else:
                    print('task', tnum, ': wait for locks')
                    sleep(1)

        ival = 5
        print('task', tnum, 'is in its critical section; sleep for', 100*ival, ' ms')
        sleep(ival/10)

        if even:
            lock2.release()
            lock1.release()
        else:
            lock1.release()
            lock2.release()

        print('task', tnum, 'is past its critical section')
        
    return True

#=========================================================================

def main():
    print('hello from main()')

    numtasks = 2
    lock1 = threading.Lock()
    lock2 = threading.Lock()
    
    thread_list = []
    
    for i in range(numtasks):
#       t = threading.Thread(target=do_work_good, args=(i, lock1, lock2))
        t = threading.Thread(target=do_work_bad, args=(i, lock1, lock2))
        thread_list.append(t)
        print('append')

    for t in thread_list:
        print('start a thread')
        t.start()

    for t in thread_list:
        t.join()

#---------------------------------------

main()
