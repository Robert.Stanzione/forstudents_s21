#include <stdio.h>
#include <stdlib.h>

typedef struct NumListNodeStruct {
  int value;
  struct NumListNodeStruct *next;
} NumListNode;

//---------------------------------------------------

int findValue(NumListNode *list, int val) {
  NumListNode *currNode, *prevNode;

  prevNode = NULL;
  currNode = list;

  while (currNode != NULL) {
    if (currNode->value == val) {
      printf("val=%d found it!\n", val);
      if (prevNode != NULL)
        printf("previous node is %d\n", val);
      else
        printf("this is the first node in the list\n");
      return 0;
    }
    prevNode = currNode;
    currNode = currNode->next;
  }

  printf("val=%d: not in list\n", val);
  return 1;
}

//------------------------------------------------------

int insertAtFront(NumListNode **list, int val) {
  NumListNode *newNode;

  newNode = (NumListNode *) malloc(sizeof(NumListNode));
  newNode->value = val;
  newNode->next = *list;
  *list = newNode;
  return 0;
}

//------------------------------------------------------

int main() {
  NumListNode *numList, *currNode;
  int rc;

  numList = NULL;

  insertAtFront(&numList, 45);
  insertAtFront(&numList, 4);
  insertAtFront(&numList, 22);

  printf("\n");

  printf("Here's the list:");
  currNode = numList;
  while (currNode != NULL) {
    printf(" %d", currNode->value);
    currNode = currNode->next;
  } 
  printf("\n");

  printf("\n");

  rc = findValue(numList, 4);
  rc = findValue(numList, 678);
  rc = findValue(numList, 22);

  return 0;
}
