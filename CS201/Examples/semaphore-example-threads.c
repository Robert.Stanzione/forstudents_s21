// example of two threads that synchronize their behavior using a semaphore
// compile and run, and you should see this:
//
// I am boss, sleeping for 5 seconds
// I am runner: waiting for sem
// I am boss: sem_post
// I am runner: done
//
// the runner sits and waits--the sem_wait() call--until the boss does
// the sem_post() call

#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#define SEM_NAME "/SEM_jhibbele"

void *boss(void *param) {
  sem_t *sem;
  int i;

  sem = (sem_t *) param;

  printf("I am boss, sleeping for 5 seconds\n");
  sleep(5);
  printf("I am boss: sem_post\n");
  sem_post(sem);
  sleep(1);

  pthread_exit(0);
}

//==================================================

void *runner(void *param) {
  sem_t *sem;
  int i;
  int value;

  sem = (sem_t *) param;

  printf("I am runner: waiting for sem\n");
  sem_wait(sem);
  printf("I am runner: done\n");

  pthread_exit(0);
}

//==================================================

int main() {
  sem_t *sem;
  pthread_attr_t attr;
  pthread_t tid1, tid2;

  sem = sem_open(SEM_NAME, O_CREAT | O_EXCL, 0666, 0);
  if (sem == SEM_FAILED) {
    printf("sem_open() failed\n");
    sem_unlink(SEM_NAME);
    return(8);
  }

  pthread_create(&tid2, NULL, boss, sem);
  sleep(1);
  pthread_create(&tid1, NULL, runner, sem);

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);

  sem_close(sem);
  sem_unlink(SEM_NAME);
  return(0);
}

