from inspect import currentframe, getframeinfo

class BP:
  def __init__(self):
    # initialize whatever you need here
    pass

  def check(self, expr):
    frameinfo = getframeinfo(currentframe().f_back)
    this_function = frameinfo.function
    this_line = frameinfo.lineno
    key = this_function + str(this_line)

    # now take action, using expr and key

    # here's a debug statement
#   print(key, expr)

  def print_summary(self):
    # print a summary
    print('summary')

#======================================================

bp_info = BP()
